<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

         <!-- common libraries -->
        <link rel="stylesheet" href="{{ asset('css/jquery-multi-selection.css')}}" />
        <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.multi-selection.v1.js') }}"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.1.3/darkly/bootstrap.min.css">
        <style>
            body {
                font-size: 1.2rem!important;
                background-color: #efefef;
                color: red;
            }
            select.form-control{
                min-height: 300px;
            }

        </style>
    </head>
    <body>
    <div class="container">
        <h1>Demo with WOO TECH</h1>
        <!-- 1 -->
        <h2>You select from left box to right</h2>
        <div class="jp-multiselect">
            <form id="demoform" action="/show-data" method="post">
            @csrf
                <div class="row" style="margin-bottom: 40px;margin-top: 40px">
                    <div class="col" style="">
                        <div class="from-panel">
                            <select name="duallistbox_from[]" class="form-control"  multiple="multiple">
                                @foreach ($leftOfLlists as $leftOfLlist)
                                    <option value="{{$leftOfLlist->id}}">{{$leftOfLlist->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="move-panel">
                            <button type="button" class="btn-move-selected-right"></button>
                            <button type="button" class="btn-move-selected-left"></button>
                        </div>
                        <div class="to-panel">
                            <select name="duallistbox_to[]" class="form-control" multiple="multiple">
                                @foreach($rightOfLlists as $rightOfLlist)
                                    <option value="{{$rightOfLlist->id}}" selected>{{$rightOfLlist->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 offset-md-6">
                        <button type="submit" class="btn btn-primary w-100">Submit(#)</button>
                    </div>
                </div>
            </form>
        </div>
        <script>
            $(".jp-multiselect").jQueryMultiSelection();
            $("#demoform").submit(function() {
                //alert($('[name="to[]"]').val());
                //alert($('[name="duallistbox_demo1[]"].selected').text());
                return true;

            });
        </script>

        </div>

    </body>

</html>
