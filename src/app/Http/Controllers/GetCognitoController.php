<?php
/*
 *  author: David Nguyen
    detail:use api aws cognito
*/

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Aws\CognitoIdentityProvider\CognitoIdentityProviderClient;
use Aws\Exception\AwsException;

class GetCognitoController extends Controller
{

    private $version;
    private $region;
    private $access_key;
    private $secret_key;
    private $client_id;
    private $client_secret;
    private $user_pool_id;


    public function getUser()
    {


        $config = [
            'version' => config("cognito.version"),
            'region'  => config("cognito.region"),
            'credentials' => [
                'key' => config("cognito.key"),
                'secret' => config("cognito.secret")
            ]
        ];

        //var_dump($config);

        $client = new CognitoIdentityProviderClient($config);

        $userPoolId = config("cognito.user_pool_id","******");

        $email = "nguyen-van-tuyen@lline-group.vn.www.lline-group.co.jp";

        try {
            $result = $client->adminGetUser([
                'UserPoolId' => $userPoolId,
                'Username' => $email  // Username
            ]);
            var_dump($result);
        } catch (AwsException $e) {
            // output error message if fails
            echo $e->getMessage() . "¥n";
            error_log($e->getMessage());
        }
    }

    public function setUserPassword()
    {

        $config = [
            'version' => config("cognito.version"),
            'region'  => config("cognito.region"),
            'credentials' => [
                'key' => config("cognito.key","AKIA5REK6NSRNDM6Z6PB"),
                'secret' => config("cognito.secret","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")
            ]
        ];
        //dd($config);
        $client = new CognitoIdentityProviderClient($config);

        $userPoolId = config("cognito.user_pool_id","******");
        //dd($userPoolId);
        $email = "nguyen-van-tuyen@lline-group.vn.www.lline-group.co.jp";

        try{
            $result = $client->adminSetUserPassword([
                'Password' => 'Tom@08122012',
                'Permanent' => true,
                'UserPoolId' => $userPoolId,
                'Username' => $email,
            ]);
        dd($result);
        } catch (AwsException $e) {
            // output error message if fails
            echo $e->getMessage() . "\n";
            error_log($e->getMessage());
        }

    }

    public function initiateAuth(){
        $config = [
            'version' => config("cognito.version"),
            'region'  => config("cognito.region"),
            'credentials' => [
                'key' => config("cognito.key","*******"),
                'secret' => config("cognito.secret","******")
            ]
        ];
        //dd($config);
        $client = new CognitoIdentityProviderClient($config);
        $email = "nguyen-van-tuyen@lline-group.vn.www.lline-group.co.jp";

        try
        {
            $result = $client->initiateAuth([
                'ClientMetadata' => [
                    'tenantId' => 'tenantId001'
                ],
                'AuthFlow' => 'USER_PASSWORD_AUTH',
                'ClientId' => config("cognito.app_client_id", "7nga6thukbo3jn4tsjlud1ij8r"),
                'UserPoolId' => config("config.user_pool_id", "ap-southeast-1_7Vlp8paAI"),
                'AuthParameters' => [
                    'USERNAME' => $email,
                    'PASSWORD' => "Tom@08122012",
                    'SECRET_HASH' => base64_encode(hash_hmac('sha256', $email . config("cognito.app_client_id", "********"), config("cognito.app_client_secret", "********"), true))
                ]

            ]);
            dd($result);

        } catch (AwsException $e) {
            // output error message if fails
            echo $e->getMessage() . "\n";
            error_log($e->getMessage());
        }
    }



    // NEW_PASSWORD_REQUIRED の場合
    public function respondToAuthChallenge(){
        $config = [
            'version' => config("cognito.version"),
            'region'  => config("cognito.region"),
            'credentials' => [
                'key' => config("cognito.key","****"),
                'secret' => config("cognito.secret","**")
            ]
        ];

        $client = new CognitoIdentityProviderClient($config);

        $email = "nguyen-van-tuyen@lline-group.vn.www.lline-group.co.jp";

        try
        {
            $result = $client->respondToAuthChallenge([
                'ChallengeName' => 'NEW_PASSWORD_REQUIRED',
                'ClientId' => config("cognito.app_client_id", "****"),
                'ChallengeResponses' => [
                    'USERNAME' => $email,
                    'PASSWORD' => 'Tom@08122012',
                    'NEW_PASSWORD' => "David@961983",
                    "userAttributes" => "{'website':'www.lline-group.co.jp'}",
                    'SECRET_HASH' => base64_encode(hash_hmac('sha256', $email . config("cognito.app_client_id", "********"), config("cognito.app_client_secret", "********"), true))
                ],
                // initiateAuthで取得可能
                'Session' => "AYABeJ92hkApnC0nuzl2U3V6foYAHQABAAdTZXJ2aWNlABBDb2duaXRvVXNlclBvb2xzAAEAB2F3cy1rbXMAUGFybjphd3M6a21zOmFwLXNvdXRoZWFzdC0xOjAzMTU3NzI0MDA0ODprZXkvYmEwNzA1YzktMTI0Mi00ODg1LWJhMmYtNDhiMWNjYTNiNDNmALgBAgEAeMtRirmB1qptVeI5EWSyPpLL6RXz-VVK9JVsLMBfSNNmAbKDx1RbaUVwteosrB8fY4cAAAB-MHwGCSqGSIb3DQEHBqBvMG0CAQAwaAYJKoZIhvcNAQcBMB4GCWCGSAFlAwQBLjARBAx7Kz8Ogn7Gr8QIKkQCARCAO165eHU8qhkihcoOjwuU5eUbUAbjDQc82Ah6Y3EM4sTQEcwDnlS18Jg-KaXhBO5rjYwP4QAugmvFqT8QAgAAAAAMAAAQAAAAAAAAAAAAAAAAALTcim08lrYHIx-N3e0zBiH_____AAAAAQAAAAAAAAAAAAAAAQAAAO4CUBMRQfeZkagQeWEw6-SA2K2AyQt31BEA7DeWeIP955dbKgTkK1eSqtfLYxwbsICWbdrdisC7lfqlDVPp7HQfnekPYqn_IEx3GKNfzEHJcDGcp-c9_kTcNflJX4vXyEUbDVVKs8eAAubbLfPnXtmipf3UetUDfY3hn3UEdopYVK7vTaLC-LopqrLE0YNXycyY_I8obzGG4pSWiozSXapyI3xEkNBalC_Md-8aIiOSw-gGmRz4t_LyCu9uBif4BvXnYLJ6cDh6CZHzzR-b-iMajHJiRZWr1FYEAVcJ830onIowuKJfmkGXnlEO_PVstnH6bDDQu4ujm0diFD55Hw",
            ]);

            dd($result);
        } catch (AwsException $e) {
            // output error message if fails
            echo $e->getMessage() . "\n";
            error_log($e->getMessage());
        }
    }

}
