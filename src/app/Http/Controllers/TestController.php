<?php

namespace App\Http\Controllers;

use App\Models\Test;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leftOfLlists = DB::table('tests')->where('delete_at', '=', null)->get();
        //dd($leftOfLlists);
        $rightOfLlists = DB::table('selected')->where('delete_at','=',null)->get();
        //dd($rightOfLlists);
        return view("test",compact('leftOfLlists','rightOfLlists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Test $test)
    {
        //$this->store($request);
        // get ids selected
        if($request->duallistbox_from){
            $ids = $request->duallistbox_from;
        }
        else{
            $ids = $request->duallistbox_to;
        }
        if($ids){
            $deleted = DB::table('selected')->whereIn('id',$ids)->delete();

            foreach ($ids as $id) {
                //echo $id;
                //$deleted_test = DB::table('tests')->where('id', '=', $id)->delete();

                //query get data from tests
                $row = DB::table('tests')->find($id);

                if($request->duallistbox_to){
                    //insert table selected
                    DB::table('selected')->updateOrInsert([
                        'id' => $id,
                        'title' => $row->title,
                        'created_at' => now(),
                        'delete_at' => null,
                    ]);
                    //update table test with colum delete_at != null
                    DB::table('tests')
                        ->where('id', $id)
                        ->update(['delete_at' => now()]);
                }
                if($request->duallistbox_from){
                    DB::table('tests') ->where('id','=',$id)->update(['delete_at' => null]);
                    DB::table('selected') ->where('id','=',$id)->update([ 'created_at' => now(),'delete_at' => now()]);
                }



            }
        }else{
            //update table test with colum delete_at != null
            DB::table('tests')->update(['delete_at' => null]);
            DB::table('selected')->update(['delete_at' => null]);
        }
        //get list of left box
        $leftOfLists = DB::table('tests')->where('delete_at', '=', null)->get();
        //get list of right box
        $rightOfLists = DB::table('selected')->where('delete_at', '=', null)->get();
        //get list of selected
        $listOfSelecteds = DB::table('tests')->whereIn('id', $ids)->get();

        return view("show",compact('leftOfLists','rightOfLists','listOfSelecteds'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Test $test)
    {
        //dd($request);
        $leftOfLlists = DB::table('tests')->get();
        $rightOfLlists = DB::table('selected')->get();
        return view("show",compact('leftOfLlists','rightOfLlists'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test)
    {
        //
    }
}
