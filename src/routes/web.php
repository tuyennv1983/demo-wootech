<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\GetCognitoController;
use App\Http\Controllers\TestController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/foo', function () {
    return 'Hello World';
});
Route::get('/test', [TestController::class, 'index']);
Route::post('/show-data', [TestController::class, 'show']);
//Route::get('/call-api', 'GetCognitoController@getUser');
Route::get('/call-api', [GetCognitoController::class, 'initiateAuth']);
Route::get('/change-pass', [GetCognitoController::class, 'setUserPassword']);
require __DIR__.'/auth.php';
